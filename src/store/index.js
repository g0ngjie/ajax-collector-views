import Vue from "vue";
import Vuex from "vuex";
import { init } from "@/common/listener";
import { getAjaxType, getGlobalSwitchOn, getProtocol } from "@/common/store";
// import logs from "./logs.json";

Vue.use(Vuex);

function getIncludes(target) {
  const list = [];
  for (const key in target) {
    if (Object.hasOwnProperty.call(target, key)) {
      const bool = target[key];
      if (bool) list.push(key);
    }
  }
  return list;
}

export default new Vuex.Store({
  getters: {
    current: (state) => state.current,
    showFooter: (state) => state.showFooter,
  },
  state: {
    logs: [],
    logMapping: {},
    // 索引使用 url列表
    urls: [],
    current: {},
    showFooter: false,
  },
  mutations: {},
  actions: {
    initListener({ state }) {
      init(false, async (res) => {
        const { type, url, method } = res;
        // 全局开关
        const enabled = await getGlobalSwitchOn();
        if (enabled) {
          const ajaxType = await getAjaxType();
          const protocol = await getProtocol();
          const _includes = getIncludes(ajaxType);
          if (_includes.includes(type)) {
            const methodIncludes = getIncludes(protocol);
            if (methodIncludes.includes(method.toLowerCase())) {
              // 去重
              if (!state.logMapping[url]) {
                state.logs.push(res);
                state.urls.push(url);
                state.logMapping[url] = res;
              }
            }
          }
        }
      });

      // debug
      // logs.forEach((item) => {
      //   if (!state.logMapping[item.url]) {
      //     state.logs.push(item);
      //     state.logMapping[item.url] = item;
      //     state.urls.push(item.url);
      //   }
      // });
    },
    reload({ state }) {
      state.logs = [];
      state.urls = [];
      state.logMapping = {};
      state.current = {};
      state.showFooter = false;
    },
    footer({ state }, bool) {
      state.showFooter = bool;
    },
    // 当前选中项
    currentClick({ state }, target) {
      state.current = target;
      state.showFooter = true;
    },
    // 搜索
    searchLogs({ state }, { keywords, isReg }) {
      if (!keywords) {
        state.logs = [];
        for (const key in state.logMapping) {
          if (Object.hasOwnProperty.call(state.logMapping, key)) {
            const target = state.logMapping[key];
            state.logs.push(target);
          }
        }
        return;
      }
      const findList = state.urls.filter((target) => {
        if (isReg) {
          const reg = new RegExp(keywords, "i");
          return reg.test(target);
        } else {
          return target.includes(keywords);
        }
      });
      state.logs = findList.map((url) => state.logMapping[url]);
    },
    remove({ state }, { url, isCurrent }) {
      // 如果是当前查看行 则隐藏
      if (isCurrent) {
        state.showFooter = false;
        state.current = {};
      }
      state.urls = state.urls.filter((target) => target !== url);
      delete state.logMapping[url];
      state.logs = [];
      for (const key in state.logMapping) {
        if (Object.hasOwnProperty.call(state.logMapping, key)) {
          const target = state.logMapping[key];
          state.logs.push(target);
        }
      }
    },
  },
});
