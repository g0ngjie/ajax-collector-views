export function init(debug = false, callback) {
  if (!chrome || !chrome.devtools) return;
  // Chrome DevTools Extension中不能使用console.log
  const log = (...args) =>
    chrome.devtools.inspectedWindow.eval(`
  console.log(...${JSON.stringify(args)});
`);

  // 注册回调，每一个http请求响应后，都触发该回调
  chrome.devtools.network.onRequestFinished.addListener(async (...args) => {
    try {
      const [
        {
          // 请求的类型，查询参数，以及url
          request: { method, queryString, url, headers: requestHeaders },
          response: { status, redirectURL, headers: responseHeaders },
          _resourceType,
          // 该方法可用于获取响应体
          getContent,
        },
      ] = args;

      // 将callback转为await promise
      // warn: content在getContent回调函数中，而不是getContent的返回值
      const content = await new Promise((res, rej) => getContent(res));

      if (debug) {
        log("[--ajax--]url", url);
        log("[--ajax--]args", args);
        log("[--ajax--]content", content);
      }

      if (["xhr", "fetch"].includes(_resourceType)) {
        callback({
          url,
          type: _resourceType,
          method,
          queryString,
          status,
          content,
          requestHeaders,
          responseHeaders,
        });
      }
    } catch (err) {
      log("[--ajax--]", err.stack || err.toString());
    }
  });
}
