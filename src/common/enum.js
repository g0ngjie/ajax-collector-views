/**
 * 存储类别
 * @enum
 */
export const StoreType = {
  /**全局开关 */
  SWITCH: "globalSwitchOn",
  /**请求协议 */
  PROTOCOL: "protocol",
  /**Ajax类型 */
  AJAX_TYPE: "ajax_type",
};
